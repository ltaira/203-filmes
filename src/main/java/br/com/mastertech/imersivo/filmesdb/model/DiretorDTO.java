package br.com.mastertech.imersivo.filmesdb.model;

import java.util.List;

public interface DiretorDTO {
	
	Long getId();
	
	String getNome();

	int getIdade();
	
	List<FilmeDTO> getFilmes();

}
